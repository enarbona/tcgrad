import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.testcontainers.containers.MySQLContainer;

public class MyTest {


    static MySQLContainer<?> mysql = new MySQLContainer<>("mysql:5.7.34");

    @BeforeAll
    static void startDb() {
        mysql.start();
        System.out.println("Mysql started");
    }

    @Test
    void test(){
        Assertions.assertTrue(true);
    }
    @AfterAll
    static void stopDb() {
        mysql.stop();
        System.out.println("Mysql stopped");
    }
}
